/* eslint-disable new-cap */
/* eslint-disable no-new */
import React, { useRef, useState } from 'react'; // JAGUNG
import {
  Form, FormGroup, Label, Button, Input, FormText
} from 'reactstrap';
// css & sweetalert
import '@sweetalert2/theme-dark';
import swal from 'sweetalert2/dist/sweetalert2';
// nextjs
import { useRouter } from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import {
  signUp,
  uploadProfileImage
} from '@/redux/reducers/register/registerSlice';
import Styles from '../styles/login.module.css';

function Register () {
  const [user, setUser] = useState({
    email: '',
    password: '',
    fullname: '',
    image: null,
    imageURL: '',
    isSuccess: false,
    fileImage: null
  });
  const fileRef = useRef();
  const navigate = useRouter();
  const dispatch = useDispatch();

  const uploadImage = (e) => {
    e.preventDefault();

    const file = fileRef.current.files[0];
    const reader = new FileReader();

    if (file) {
      reader.addEventListener('load', () => {
        setUser({
          ...user,
          image: reader.result,
          fileImage: file,
          imageURL: `images/${file.name}`
        });
      }, false);

      if (file.type.includes('image/')) reader.readAsDataURL(file);
    } else {
      // handle if picture has been removed
      setUser({ image: null });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser((prevValue) => {
      const updatedValue = { ...prevValue, [name]: value };
      return updatedValue;
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      e.preventDefault();

      swal.fire({
        title: 'Registering...',
        text: 'wait for a second...',
        timer: 5000,
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false
      });

      const imageStatus = await dispatch(uploadProfileImage({
        fileImage: user.fileImage
      }));

      const registerStatus = await dispatch(signUp({
        email: user.email,
        password: user.password,
        fullname: user.fullname,
        photoURL: user.imageURL
      }));

      if (imageStatus.meta.requestStatus === 'fulfilled' && registerStatus.meta.requestStatus === 'fulfilled') {
        setUser({ ...user, isSuccess: true });
      } else {
        new swal({
          icon: 'error',
          title: 'catch',
          text: 'something went wrong !!'
        });
      }

      if (user.isSuccess === true) {
        new swal({
          title: 'Successfully Registered!',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false
        });
      }

      return navigate.push('/login');
    } catch (error) {
      const errorMessage = error.message;

      new swal({
        icon: 'error',
        title: 'catch',
        text: `something went wrong !! ${errorMessage}`
      });
    }
  };

  return (
    <div className="Login-component">

      <Head>
        <title>Register</title>
        <link rel="icon" type="image/png" href="/game-images/logo 1.png" />
      </Head>

      <div className={`${Styles.background_register}`} style={{ marginTop: '50px' }}>
        <h5 className="text-center text-white ">Register</h5>
        <Form onSubmit={handleSubmit}>
          <div className="d-flex justify-content-center">
            {user.image === null
              ? (
                <img
                  src="./default_profile_icon.png"
                  alt=""
                  style={{
                    width: '150px',
                    height: '150px',
                    borderRadius: '100px',
                    marginBottom: '20px',
                    marginTop: '20px'
                  }}
                />
                )
              : (
                <img
                  src={user.image}
                  alt=""
                  style={{
                    width: '150px',
                    height: '150px',
                    borderRadius: '100px',
                    marginBottom: '20px',
                    marginTop: '20px'
                  }}
                />
                )}
          </div>
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="email">Email</Label>
            <Input
              id="email"
              name="email"
              placeholder="Email"
              type="email"

              value={user.email}
              onChange={handleChange}
            />
          </FormGroup>
          {' '}
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="password">Password</Label>
            <Input
              id="password"
              name="password"
              placeholder="Password"
              type="password"

              value={user.password}
              onChange={handleChange}
            />
          </FormGroup>
          {' '}
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="fullname">Fullname</Label>
            <Input
              id="fullname"
              name="fullname"
              placeholder="Fullname"
              type="text"
              value={user.fullname}
              onChange={handleChange}
            />
          </FormGroup>
          {' '}
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="profilePicture">Profile Picture</Label>
            <Input
              id="pictureProfile"
              name="file"
              type="file"
              accept="image/*"
              innerRef={fileRef}
              onChange={uploadImage}
            />
            <FormText>
              upload your profile image here. *you can skip this and use default image
            </FormText>
          </FormGroup>
          {' '}
          <div className={`${Styles.center}`}>
            <Button className={`${Styles.button} text-center mb-4`}>Register</Button>
            <Link className="text-white" href="/login">Already have an account? Login here</Link>
            <br />
            <Link className="text-white" href="/">Back</Link>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default Register;
