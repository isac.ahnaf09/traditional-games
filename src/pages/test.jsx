import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../services/firebase';
import { setEmail } from '../redux/reducers/login/loginSlice';

// const inter = Inter({ subsets: ['latin'] });

export default function Home () {
  const email = useSelector((state) => state.login.email);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        dispatch(setEmail(user.email));
      }
    });
    return unsubscribe;
  }, [dispatch]);

  return (
    <h1 className="text-white">
      Saya adalah
      {email}
    </h1>
  );
}
