import React, { useState } from 'react';
import {
  Form, FormGroup, Label, Button, Input
} from 'reactstrap';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import Head from 'next/head';
import Link from 'next/link';
import { signIn } from '../redux/reducers/login/loginSlice';
import Styles from '../styles/login.module.css';

function Login () {
  const dispatch = useDispatch();
  const navigate = useRouter();
  const [user, setUser] = useState({
    email: '',
    password: ''
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      dispatch(signIn(user));
      navigate.push('/home');
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser((prevValue) => {
      const updatedValue = { ...prevValue, [name]: value };
      return updatedValue;
    });
  };

  return (
    <div className="Login-component">
      <Head>
        <title>Login</title>
        <link rel="icon" type="image/png" href="/game-images/logo 1.png" />
      </Head>
      <div className={`${Styles.background}`}>
        <h5 className="text-center text-white ">Account Login</h5>
        {/* <div>{passwordError}</div> */}
        <Form onSubmit={handleSubmit}>
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="exampleEmail">Email</Label>
            <Input
              id="username"
              name="email"
              placeholder="Email"
              type="text"
              value={user.email}
              onChange={handleChange}
            />
          </FormGroup>
          <FormGroup className={`${Styles.center} text-white`}>
            <Label for="examplePassword">Password</Label>
            <Input
              id="examplePassword"
              name="password"
              value={user.password}
              placeholder="Password"
              type="password"
              onChange={handleChange}
            />
          </FormGroup>
          <div className={`${Styles.center}`}>
            <Button className={`${Styles.button} text-center mb-4`}>Login</Button>
            <Link className="text-white d-flex mb-2" href="/">Back</Link>
            <Link className="text-white" href="/register">Dont have an account? Register here</Link>
            <br />
          </div>
        </Form>
      </div>
    </div>
  );
}

export default Login;
