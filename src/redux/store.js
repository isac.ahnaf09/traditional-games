import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './reducers/login/loginSlice';
import registerSlice from './reducers/register/registerSlice';

export const store = configureStore({
  reducer: {
    login: loginReducer,
    register: registerSlice
  }
});
