import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';
import { FaStar, FaSignOutAlt } from 'react-icons/fa';
import { Container, Row, Col } from 'reactstrap';
import Head from 'next/head';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import {
  collection,
  getDocs,
  query,
  where
} from 'firebase/firestore';
import { getDownloadURL, getStorage, ref } from 'firebase/storage';
import { auth, db } from '../services/firebase';
import { setEmail } from '../redux/reducers/login/loginSlice';
import JsPDF from 'jspdf';

function Sidebar ({ children }) {
  const email = useSelector((state) => state.login.email);
  const dispatch = useDispatch();
  const [userData, setUserData] = useState({
    image: '',
    fullname: ''
  });

  const getDataFromFirestore = async (userEmail) => {
    let data = {
      imageURL: '',
      fullname: ''
    };
    const querySnapshot = await getDocs(query(collection(db, 'users'), where('email', '==', userEmail)));
    querySnapshot.forEach((doc) => {
      data = {
        imageURL: doc.data().photoURL,
        fullname: doc.data().fullname
      };
    });

    if (data.imageURL) {
      const storage = getStorage();
      const storageRef = ref(storage, data.imageURL);
      getDownloadURL(storageRef)
        .then((url) => {
          setUserData({
            ...userData,
            image: url,
            fullname: data.fullname
          });
        })
        .catch((error) => console.log(error));
    }
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        getDataFromFirestore(user.email);
        dispatch(setEmail(user.email));
      }
    });
    return unsubscribe;
  }, [dispatch]);

  const handleSignOut = () => {
    signOut(auth).then(() => {
      localStorage.removeItem('token');
      dispatch(setEmail(''));
    });
  };

  const generateCertificate = () => {
    const doc = new JsPDF('landscape');
    const participantName = userData.fullname;

    // Certificate Dimensions
    const certificateWidth = 300;
    const certificateHeight = 200;
    const centerX = certificateWidth / 2;
    const centerY = certificateHeight / 2;

    // Draw Certificate Border
    doc.rect(10, 10, certificateWidth - 20, certificateHeight - 10);

    // Add Image
    const imgWidth = 300;
    const imgHeight = 200;
    doc.addImage('./certificate-template-2.png', 'PNG', 0, 10, imgWidth, imgHeight);

    // Add Recipient Name
    doc.setFontSize(34);
    doc.setFont('bold');
    doc.text(participantName.toUpperCase(), centerX, centerY - 10, 'center');

    doc.save('bravery medals.pdf');
  };

  return (
    <>
      <Head>
        <link rel="stylesheet" href="/css/sidebar.css" />
      </Head>
      <Container fluid>
        <Row>
          <Col xs="2">
            <nav className="sidebar">
              <ul className="sidebar__list">
                <li className="text-center">
                  <img src={userData.image} className="rounded-circle profile-picture mb-3" alt="Profile" />
                </li>
                <li className="text-center profile">{userData.fullname}</li>
                <li className="text-center profile mb-5">{email}</li>
                <li>
                  <Link href="/" className="sidebar__link">
                    <FaStar className="star-icon" />
                    {' '}
                    Home
                  </Link>
                </li>
                <li>
                  <Link href="/gamelist" className="sidebar__link">
                    <FaStar className="star-icon" />
                    {' '}
                    List Game
                  </Link>
                </li>
                <li>
                  <Link href="/users" className="sidebar__link">
                    <FaStar className="star-icon" />
                    {' '}
                    List Profile
                  </Link>
                </li>
                <li>
                  <Link href="/leaderboard" className="sidebar__link">
                    <FaStar className="star-icon" />
                    {' '}
                    Leaderboard
                  </Link>
                </li>
                <li>
                  <Link href="#" onClick={generateCertificate} className="sidebar__link">
                    <FaStar className="star-icon" />
                    {' '}
                    Download Certificate
                  </Link>
                </li>
                <li>
                  <Link href="/" className="logout" onClick={handleSignOut}>
                    <FaSignOutAlt className="star-icon" />
                    {' '}
                    Logout
                  </Link>
                </li>
              </ul>
            </nav>
          </Col>
          <Col xs="10">{children}</Col>
        </Row>
      </Container>
    </>
  );
}

Sidebar.propTypes = {
  children: PropTypes.node.isRequired
};

export default Sidebar;
