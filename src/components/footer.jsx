import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';

export default function Footer ({ children }) {
  return (
    <>
      {children}
      <footer className="text-white" id="footer">
        <ul className="nav justify-content-end">
          <li className="nav-item">
            <a className="nav-link active ms-3" aria-current="page" href="#main-screen">
              Main
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#about">
              About
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#features">
              Game Feature
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#requirements">
              System requirements
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#top-scores">
              Quotes
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#footer">
              <img src="./images/facebook.svg" alt="facebook" />
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link ms-3" href="#footer">
              <img src="./images/twitter.svg" alt="twitter" />
            </a>
          </li>
          <li className="nav-item ms-3">
            <a className="nav-link disabled" href="#footer">
              <img src="./images/vector.svg" alt="youtube" />
            </a>
          </li>
          <li className="nav-item ms-3">
            <a className="nav-link disabled" href="#footer">
              <img src="./images/twitch.svg" alt="twitch" />
            </a>
          </li>
        </ul>
        <hr />
        <div id="copyright">
          <nav className="navbar navbar-expand-lg navbar-dark">
            <div className="container">
              <a className="navbar-nav mb-2 mb-lg-0" href="#footer" id="copyright-2018">
                © 2018 Your Games, Inc. All Rights Reserved
              </a>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0" />
                <ul className="navbar-nav d-flex">
                  <li className="nav-item">
                    <a className="nav-link active" aria-current="page" href="#footer">
                      privacy policy
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#footer">
                      terms of service
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#footer">
                      code of conduct
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </footer>
    </>
  );
}

Footer.propTypes = {
  children: PropTypes.node.isRequired
};
