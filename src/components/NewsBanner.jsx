import React from 'react';
import ReactPlayer from 'react-player';

const YoutubeEmbed = () => (
  <ReactPlayer
    url="https://youtu.be/uGHrNUgnWVY"
    controls="true"
    playing="true"
    muted
    loop="true"
  />
);

export default YoutubeEmbed;
