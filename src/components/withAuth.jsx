import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useRouter } from 'next/router';
import { auth } from '../services/firebase';

const withAuth = (Component) => {
  function AuthComponent (props) {
    const [user, loading] = useAuthState(auth);
    const router = useRouter();

    if (loading) {
      return <div>Loading...</div>;
    }

    if (!user) {
      router.push('/login');
      return null;
    }

    return <Component {...props} />;
  }

  return AuthComponent;
};

export default withAuth;
